import React, { Component } from 'react';
import Header from './header'
import Footer from './footer'
import Home from'./home';
import About from './AboutComponent ';

import {Switch,Route,Redirect} from 'react-router-dom';
import DishDetail from './DishdetailComponent'


import MenuDISH from './MenuComponent'
import Contact from './contact'
import { PROMOTIONS } from '../shared/promotions';
import { LEADERS } from '../shared/leaders';

import { DISHES } from '../shared/dishes';
import { COMMENTS } from '../shared/comments';

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dishes: DISHES,
            promotions: PROMOTIONS,
            leaders: LEADERS,
            comments:COMMENTS,
            selectedDish:null
        };
    }
getSelected(dish){

    this.setState({selectedDish:dish})
}

render(){
    const HomePage=()=>{
        return(
            <Home dish={this.state.dishes.filter((dish) => dish.featured)[0]} promotion={this.state.promotions.filter((promo) => promo.featured)[0]} leader={this.state.leaders.filter((leader) => leader.featured)[0]} />
        );
    }
    const RenderLeader=()=>{

        return (

            <About leaders={this.state.leaders}/>


        );
    }
    

    const DishWithId = ({ match }) => {
        return (
            <DishDetail dish={this.state.dishes.filter((dish) => dish.id === parseInt(match.params.id, 10))[0]}
                comment={COMMENTS.filter((comment) => comment.id === parseInt(match.params.leaderID, 10))[0]} />
        );
    };
    return (
        <div>


        <Header/>
    <Switch>

                <Route path="/home" component={HomePage}/>
                <Route exact path="/menu" component={() => <MenuDISH dishes={this.state.dishes} comments={this.state.comments} selectedDish={this.state.selectedDish} GetSelected={(dish) => { this.getSelected(dish) }} /> }   />
                <Route exact path="/about" component={RenderLeader} />

                <Route path='/menu/:id' component={DishWithId} />


                <Route exact path="/contact" component={Contact} />

<Redirect to="/home"/>

    </Switch>
<Footer/>
    </div >
)
}
}


export default Main;