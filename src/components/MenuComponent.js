import React, {Component } from 'react';
import {
    Card, CardImg, CardImgOverlay,
    CardTitle,Breadcrumb,BreadcrumbItem
} from 'reactstrap';
import {Link} from 'react-router-dom'

function RenderItem(props) {

    return (
        <div className="col-12 col-md-15 m-5">
            <Card>
                <Link to={`/menu/${props.dish.id}`} >
                    <CardImg width="100%" src={props.dish.image}  />
                    <CardImgOverlay>
                        <CardTitle>{props.dish.name}</CardTitle>
                    </CardImgOverlay>
                </Link>
            </Card>
        </div>
    );




}
class MenuDISH extends Component { 
 
    render() {
     const menu = this.props.dishes.map((dish) => {

return(

    <div className="col-12 col-md-5 m-1" key={dish.id}>

        <RenderItem dish={dish}/>

</div>

    );
}

);
        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
                        <BreadcrumbItem active>Menu</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>Menu</h3>
                        <hr />
                    </div>
                </div>
                <div className="row">
                    {menu}
                </div>
            </div>
        );
    }
}
export default MenuDISH;