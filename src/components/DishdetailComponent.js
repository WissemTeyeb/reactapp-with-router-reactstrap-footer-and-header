import React, { Component } from 'react';
import {
    Card, CardImg, CardText, CardBody,
    CardTitle,BreadcrumbItem,Breadcrumb,
} from 'reactstrap';
import {Link} from 'react-router-dom'

function RenderComment(props) {

    if (props.comment != null) {
        return (
            <div className="col-12 col-md-5 md-1 ">
                <Card>

                    <CardTitle><h4>Comments</h4></CardTitle>
                    <CardBody>
                        <ul class="list-unstyled">
                            <li>{props.comment.comment}</li>
                            <li>--{props.comment.author},{props.comment.date}</li>


                        </ul>          
                          </CardBody>
                </Card>

            </div >


        )
    }
    else {
        return (
            <div></div>
        )
    }


}
function RenderDish(props) {
    return (
        <div className="col-12 col-md-5 md-1 ">
            <Card>
                <CardImg top src={props.dish.image} alt={props.dish.name} />
                <CardBody>
                    <CardTitle>{props.dish.name}</CardTitle>
                    <CardText>{props.dish.description}</CardText>
                </CardBody>
            </Card>
        </div>
    );

}

class DishDetail  extends Component {


   
   

render(){



    return (
        <div className="container">
            <div className="row">
                <Breadcrumb>

                    <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                    <BreadcrumbItem active>{this.props.dish.name}</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>{this.props.dish.name}</h3>
                    <hr />
                </div>
            </div>
            <div className="row">



                    <RenderDish dish={this.props.dish} />

                    <RenderComment comment={this.props.comment} />
            </div>

  
            </div>
      
    );
     }
     }
    
     
     
     
     
export default DishDetail ;